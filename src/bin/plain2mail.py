import plain
from email.message import EmailMessage
import subprocess
import argparse
from pathlib import Path
import sys

parser = argparse.ArgumentParser(description="Send the contents of plain files as mail via the local MTA.")
parser.add_argument("plain", nargs=argparse.REMAINDER, help="The plain files or directories thereof to send from.")
args = parser.parse_args(sys.argv[1:])

for name in args.plain:
    to = None; cc = None; subject = None # Python doesn't create an inner scope for a loop, so we have to clear these to prevent confusion.
    path = Path(name)
    file = plain.File(path)

    if "to" in file.keys:
        to = file.keys["to"].value
        file.remove_key("to")
    if "cc" in file.keys:
        cc = file.keys["cc"].value
        file.remove_key("cc")

    if "subject" in file.keys:
        subject = file.keys["subject"].value
        file.remove_key("subject")
    elif "title" in file.keys:
        subject = file.keys["title"].value
        file.remove_key("title")
    elif "name" in file.keys:
        subject = file.keys["name"].value
        file.remove_key("name")
    elif "label" in file.keys:
        subject = file.keys["label"].value
        file.remove_key("label")
    else:
        subject = path.name

    body = str(file)
    message = EmailMessage()
    message.set_content(body)

    if to:
        message["To"] = to
    if cc:
        message["Cc"] = cc
    message["Subject"] = subject

    # sendmail -t -oi reads a single complete message from stdin, ignoring . lines, and uses the recipients in the existing headers.
    subprocess.Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=subprocess.PIPE).communicate(message.as_bytes())
