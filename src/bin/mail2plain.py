import plain
from email.message import EmailMessage
import subprocess
import argparse
from pathlib import Path
import sys
import os
import mailbox

parser = argparse.ArgumentParser(description="Retrieve mail for the current user via the local MTA and write the contents to plain files.")
parser.add_argument("-s", "--subject", help="A tag that must be included in the subject.")
parser.add_argument("-a", "--address", help="A tag that must be present in a recipient address, usually a () comment or + alias.")
parser.add_argument("-c", "--check", help="The interval between rechecks.")
parser.add_argument("-d", "--dead", help="The file to write bad messages to.")
parser.add_argument("plain", help="The directory to write received data to.")
args = parser.parse_args(sys.argv[1:])

def deadletter(spool, key, message):
    if args.dead != None:
        archive = mailbox.mbox(args.dead)
        archive.add(message)
        archive.close()

def nextfile(dir):
    highest_num = 0
    for child in dir.iterdir():
        try:
            this_num = int(child.name)
        except: # Ignore filenames that aren't integers.
            continue
        if this_num > highest_num:
                highest_num = this_num

    file_num = highest_num + 1
    path = None
    while path == None: # Deal with race condition.
        try:
            path = dir / str(file_num)
            path.open("x").close()
            return path
        except FileExistsError:
            file_num += 1
            continue

spool = mailbox.mbox(os.getenv("MAIL"))

to_discard = []

for key, message in spool.iteritems():
    try:
        path = None # Python doesn't do inner scopes. Reset this variable so exception handler doesn't delete the wrong file.
        
        if args.subject != None:
            if message["Subject"] != None and not args.subject in message["Subject"]:
                continue
            
        if args.address != None:
            if (message["To"] != None and not args.address in message["To"]) or (message["Cc"] != None and not args.address in message["Cc"]) or (message["Bcc"] and not args.address in message["Bcc"]):
                continue
                
        path = nextfile(Path(args.plain))
                
        file = plain.File(path, message.get_payload())
        
        if "From" in message:
            file.data.insert(0, plain.Key("from", message["From"]))
        if "To" in message:
            file.data.insert(0, plain.Key("to", message["To"]))
        if "Cc" in message:
            file.data.insert(0, plain.Key("cc", message["Cc"]))
        if "Subject" in message:
            file.data.insert(0, plain.Key("subject", message["Subject"]))
        if "Date" in message:
            file.data.insert(0, plain.Key("date", message["Date"]))

        file.writeout()
        to_discard.append(key)
    except Exception as e:
        try:
            print("Error processing message with ID " + message["Message-Id"] + " (dead-lettering): " + str(e))
            if path != None: # Delete file created for the data.
                path.unlink()
            deadletter(spool, key, message)
            to_discard.append(key)
        except Exception as e:
            print("Double fault: " + str(e))
        

for key in to_discard:
    spool.discard(key)

spool.close()
