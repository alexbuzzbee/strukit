import sqlite3
import plain
import sqlnames
import argparse
from pathlib import Path
import sys

parser = argparse.ArgumentParser(description="Copy data from a db to plain files.")
parser.add_argument("-b", "--begin", default="0", help="Only copy records with a sequence column value equal to or greater than this value.")
parser.add_argument("-e", "--end", default="0", help="Only copy records with a sequence column value equal to or less than this value.")
parser.add_argument("-s", "--seq", default="ROWID", help="Use COLUMN as the sequence column.")
parser.add_argument("-V", "--verbose", action="store_true", help="Print the path of each file (to stderr) as it is written.")
parser.add_argument("-D", "--debug", action="store_true", help="Enable debugging in the argument parser, plain parser, and SQL generator.")
parser.add_argument("db", help="The db to copy from.")
parser.add_argument("table", help="The table to copy from.")
parser.add_argument("plain", help="The directory to copy to.")
args = parser.parse_args(sys.argv[1:])

if args.debug:
    plain.debug = True
    print("DEBUG: begin = " + str(args.begin))
    print("DEBUG: end = " + str(args.end))
    print("DEBUG: seq = " + str(args.seq))
    print("DEBUG: debug = " + str(args.debug))
    print("DEBUG: db = " + str(args.db))
    print("DEBUG: table = " + str(args.table))
    print("DEBUG: plain = " + str(args.plain))

connection = sqlite3.connect(args.db)
connection.row_factory = sqlite3.Row
cursor = connection.cursor()

table = sqlnames.sanitize_table(connection, args.table)
seq = sqlnames.sanitize_field(connection, table, args.seq)

if args.begin or args.end:
    where = " WHERE"
    if args.begin:
        where += f" {seq} >= ?"
    if args.end:
        where += f" {seq} <= ?"
else:
    where = ""

selection = f"SELECT ROWID, * FROM {table}{where};"
if args.debug: print("DEBUG: " + selection)
cursor.execute(selection, [args.begin])

for row in cursor:
    if type(row[1]) is str:
        path = Path(args.plain) / row[1]
    else:
        path = Path(args.plain) / str(row["ROWID"])
    file = plain.File(path)
    
    if args.verbose or args.debug: print(str(path))

    for column in row.keys():
        if column == "rowid" or row[column] is None: pass
        elif column == "body":
            file.add_body(plain.Body(row[column]))
        else:
            file.add_key(plain.Key(column, str(row[column])))

    if args.debug: print("DEBUG: Writing")
    file.writeout()
