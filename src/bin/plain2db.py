import plain
import sqlnames
import sqlite3
import argparse
from pathlib import Path
import sys

parser = argparse.ArgumentParser(description="Copy data from plain files to db.")
parser.add_argument("-d", "--delete", action="store_true", help="Delete the copied plain files once all data is committed.")
parser.add_argument("-s", "--since", default="1970-01-01", help="Only copy data in files updated at or after this date.")
parser.add_argument("-V", "--verbose", action="store_true", help="Print the path of each file (to stderr) as it is read.")
parser.add_argument("-D", "--debug", action="store_true", help="Enable debugging in the argument parser, plain parser, and SQL generator.")
parser.add_argument("db", help="The db to copy to.")
parser.add_argument("table", help="The table to copy to.")
parser.add_argument("plain", nargs=argparse.REMAINDER, help="The plain files or directories thereof to copy from.")
args = parser.parse_args(sys.argv[1:])

if args.debug:
    plain.debug = True
    print("DEBUG: delete = " + str(args.delete))
    print("DEBUG: since = " + str(args.since))
    print("DEBUG: verbose = " + str(args.verbose))
    print("DEBUG: debug = " + str(args.debug))
    print("DEBUG: db = " + str(args.db))
    print("DEBUG: table = " + str(args.table))
    print("DEBUG: plain = " + str(args.plain))
    
min_time = 0 # TODO: Calculate minimum timestamp.

paths = []

# Find files to read.
for name in args.plain:
    path = Path(name)
    if path.is_dir():
        for child in path.iterdir():
            if child.is_file():
                paths.append(child)
    elif path.is_file():
        paths.append(path)
    else:
        print(str(path) + ": is not a regular file or a directory", file=sys.stderr)

# Prepare database connection.
connection = sqlite3.connect(args.db)
cursor = connection.cursor()

table = sqlnames.sanitize_table(connection, args.table)

# Read the files and write the database.
for path in paths:
    if args.verbose:
        print(str(path), file=sys.stderr)
    
    if path.stat().st_mtime < min_time:
        continue
    
    file = plain.File(path)
    data = {}
    for element in file.data:
        if isinstance(element, plain.Key):
            data[element.key] = element.value
        elif isinstance(element, plain.Body):
            data["body"] = element.text
        else:
            print(str(path) + ": unknown data", file=sys.stderr)

    if len(data.keys()) == 0: # Don't insert empty files.
        continue
    
    # Generate the INSERT query text.
    columns = []
    for key in data.keys():
        columns.append(sqlnames.sanitize_field(connection, table, key))
    columns = ", ".join(columns)
    values = "?" + (", ?" * (len(data) - 1))
    insertion = f"INSERT INTO {table} ({columns}) VALUES ({values});"
    if args.debug: print("DEBUG: SQL " + insertion)

    # Insert the data.
    cursor.execute(insertion, list(data.values()))

connection.commit()

# Delete the files if specified.
if args.delete:
    for path in paths:
        path.delete()
