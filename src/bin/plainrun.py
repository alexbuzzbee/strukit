import subprocess
import shlex
import argparse
import sys
import re
from pathlib import Path
import fwatch
import plain

parser = argparse.ArgumentParser(description="Wait for a new plain file to appear and run a command.")
parser.add_argument("-k", "--key", action="append", help="Substitute the value of this key for the next percent-k in the command.")
parser.add_argument("plain", help="The plain directory to watch for new files.")
parser.add_argument("command", help="The command to run, with the file path substituted for percent-p.")
args = parser.parse_args(sys.argv[1:])

def substitute_command(command, path, keys):
    command = command.replace("%p", shlex.quote(str(info.path)))
    for key in keys:
        command = re.sub("%k", shlex.quote(key.value), command)
    return command
    
while True:
    changes = fwatch.wait(args.plain)
    for info in changes:
        if info.change_type == "created":
            file = plain.File(info.path)
            
            key_substs = []
            for key in args.key:
                key_substs.append(file.keys[key])
                
            command = shlex.split(substitute_command(args.command, info.path, key_substs))
            subprocess.run(command)
