import sqlnames
import sqlite3
import tkinter as tk
import argparse
from pathlib import Path
import sys
import re

parser = argparse.ArgumentParser(description="Copy data from a db to plain files.")
parser.add_argument("-f", "--form", help="Use the formspec in this file.")
parser.add_argument("-D", "--debug", action="store_true", help="Enable debugging in the argument parser, plain parser, and SQL generator.")
parser.add_argument("db", help="The db to copy from.")
parser.add_argument("table", help="The table to copy from.")
parser.add_argument("rowid", nargs="?", help="The ID of the record to edit. If omitted, create a new record.")
args = parser.parse_args(sys.argv[1:])

if args.debug:
    print(f"DEBUG: form = {args.form}")
    print(f"DEBUG: db = {args.db}")
    print(f"DEBUG: table = {args.table}")
    print(f"DEBUG: rowid = {args.rowid}")

CELL = re.compile("\"(.*)\" \((.*)\)")

class GridCell:
    """
    A section on the grid specified by the formspec.
    """
    row = 0
    col = 0
    width = 1
    height = 1
    label = ""
    field = ""
    widget = None
    label_widget = None

    def add_to(self, form):
        if self.height > 1:
            self.widget = tk.Text(form)
        else:
            self.widget = tk.Entry(form)
        # Widget is double-tall, but with the top row reserved for the label.
        self.widget.grid(row=(self.row * 2) + 1, rowspan=1 + ((self.height - 1) * 2), column=self.col, columnspan=self.width, sticky=tk.N+tk.E+tk.S+tk.W)
        self.label_widget = tk.Label(form, text=self.label)
        self.label_widget.grid(row=self.row * 2, column=self.col, sticky=tk.W)
    
    def set_content(self, text):
        if self.height > 1:
            index = "1.0"
        else:
            index = 0
        if text is None: # Even if explicit.
            text = ""
        self.widget.delete(index, tk.END)
        self.widget.insert(index, text)

    def get_content(self):
        if self.height > 1:
            return self.widget.get("1.0", tk.END)
        else:
            return self.widget.get()
    
    def __init__(self, row, col, label, field):
        self.row = row
        self.col = col
        self.label = label
        self.field = field

# Process formspec.
with Path(args.form).open("r") as formspec:
    row = 0
    col = 0
    grid = {}
    cells = []
    for line in formspec:
        col = 0
        grid[row] = {}
        for text in line.split("\t"):
            match = CELL.match(text)
            if match is not None:
                cell = GridCell(row, col, match[1], match[2])
                grid[row][col] = cell
                cells.append(cell)
            else:
                if text.strip() == "<":
                    if col == 0:
                        print(f"Error: Extension points out of form at row {row} column {col}.", file=sys.stderr)
                        exit(1)
                    cell = grid[row][col - 1]
                    cell.width += 1
                    grid[row][col] = cell
                elif text.strip() == "^":
                    if row == 0:
                        print(f"Error: Extension points out of form at row {row} column {col}.", file=sys.stderr)
                        exit(1)
                    cell = grid[row - 1][col]
                    cell.height += 1
                    grid[row][col] = cell
                else:
                    print(f"Error: Invalid contents {text.strip()} in formspec at row {row} column {col}.", file=sys.stderr)
                    exit(1)
            col += 1
        row += 1

connection = sqlite3.connect(args.db)
connection.row_factory = sqlite3.Row

table = sqlnames.sanitize_table(connection, args.table)

# Create new default record if ROWID not given.
if not args.rowid:
    cursor = connection.cursor()
    cursor.execute(f"INSERT INTO {table} DEFAULT VALUES;")
    rowid = cursor.lastrowid
    cursor.close()
else:
    rowid = args.rowid

# Get data.
cursor = connection.cursor()
cursor.execute(f"SELECT * FROM {table} WHERE ROWID = ?;", [rowid])
data = cursor.fetchone()
cursor.close()
if data is None:
    print(f"Error: No record {rowid} in {args.table}.", file=sys.stderr)
    exit(2)

class Form(tk.Frame):
    """
    A form showing controls supplied by GridCell objects.
    """
    cells = {}
    reset_data = {}

    def __init__(self, cells, data, master=None):
        super().__init__(master)
        self.master = master
        self.cells = {}
        self.reset_data = {}

        for cell in cells:
            cell.add_to(self)
            self.cells[cell.field] = cell
            cell.set_content(data[cell.field])
            self.reset_data[cell.field] = data[cell.field]

        self.pack()

    def save(self):
        if args.debug: print("DEBUG: save")
        edited_data = {}
        for field, cell in self.cells.items():
            content = cell.get_content()
            edited_data[field] = content
            if args.debug: print(f"DEBUG: {field} = {content}")
        self.reset_data = edited_data

        field_names = list(edited_data)
        field_values = []
        for name in field_names:
            field_values.append(edited_data[name])
        
        set_list_items = []
        for field in field_names:
            sanitized_field = sqlnames.sanitize_field(connection, table, field)
            set_list_items.append(f"{sanitized_field}=?")
        set_list = ", ".join(set_list_items)

        cursor = connection.cursor()
        query = f"UPDATE {table} SET {set_list} WHERE ROWID = ?;"
        if args.debug:
            print("DEBUG: " + query)
            print(f"DEBUG: ROWID = {args.rowid}")
        cursor.execute(query, field_values + [rowid])
        if args.debug: print(f"DEBUG: {cursor.rowcount} rows")
        cursor.close()
        connection.commit()
    
    def reset(self):
        for field, value in self.reset_data.items():
            self.cells[field].set_content(value)

class Window(tk.Frame):
    """
    A window containing a Form and buttons for controlling it.
    """
    form = None
    save_btn = None
    close_btn = None
    reset_btn = None

    def __init__(self, *args, master=None, **kwargs):
        super().__init__(master)
        self.master = master

        self.form = Form(*args, **kwargs, master=self)
        self.form.grid(row=0, column=0, columnspan=3, sticky=tk.N+tk.E+tk.S+tk.W)
        self.save_btn = tk.Button(master=self, text="Save", command=self.form.save)
        self.save_btn.grid(row=1, column=0)
        self.reset_btn = tk.Button(master=self, text="Reset", command=self.form.reset)
        self.reset_btn.grid(row=1, column=1)
        self.close_btn = tk.Button(master=self, text="Close", command=self.master.destroy)
        self.close_btn.grid(row=1, column=2)

        self.pack()

# Create and show form window.
root = tk.Tk()
root.title("Database record")
window = Window(cells, data, master=root)
window.mainloop()
