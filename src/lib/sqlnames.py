# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
StruKit SQLite identifier validation/sanitization module. The functions in this module read from the database, so it is advisable to perform sanitizations only once and reuse the results. Results for the same names may differ between different database files.
"""
import sqlite3

def quote_sql_name(name):
    """
    Quotes a SQL identifier and escapes any internal quotes.

    WARNING: This function alone may not be sufficient to prevent SQL injection. Use sanitize_field() for field names or sanitize_table() for table names.
    """
    return '"' + name.replace('"', '""') + '"'

def sanitize_table(db, name):
    """
    Verifies that a string names a table in db, then quotes it and returns the result. Raises ValueError if the name is not a table name.
    """
    curs = db.cursor()
    curs.execute("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name = ?;", [name])
    if curs.fetchone() is None:
        curs.close()
        raise ValueError(f"{name}: Table not found.")
    curs.close()
    
    return quote_sql_name(name)

def sanitize_field(db, sanitized_table_name, name):
    """
    Verifies that a string names a field in the specified table, which MUST be given as an ALREADY SANITIZED table name (using sanitize_table()) if not hard-coded to a known-safe value, then quotes it and returns the result. Raises ValueError if the name is not a field name.
    """
    if name.upper() == "ROWID": # ROWID doesn't show up in description.
        return '"ROWID"'
    curs = db.cursor()
    curs.execute(f"SELECT * FROM {sanitized_table_name} LIMIT 0;")
    for field in curs.description: # Description provides a list of tuples whose first values are the field names from the last query, even if it had zero results.
        if field[0] == name:
            curs.close()
            return quote_sql_name(name)
    curs.close()
    raise ValueError(f"{name}: Field not found.")
