import os
import sys 
import time
from pathlib import Path

class ChangeInfo:
    def __init__(self, path, change_type, detected_with):
        self.path = path
        self.change_type = change_type
        self.detected_with = detected_with

def __await_change_polling(path):
    orig_mtime = os.stat(path).st_mtime
    while True:
        time.sleep(1)
        if os.stat(path).st_mtime != orig_mtime:
            return ChangeInfo(path, "modified", "polling")

def __await_change(path):
    # TODO: Pick a change-detection mechanism.
    __await_change = __await_change_polling
    return __await_change(path)

def wait(path):
    path = Path(path)
    if path.is_dir():
        orig_children = [child for child in path.iterdir()]
    changes = [__await_change(path)]
    if path.is_dir():
        children = [child for child in path.iterdir()]
        for orig_child in orig_children:
            if not orig_child in children:
                changes.append(ChangeInfo(orig_child, "removed", "dir_collateral"))
        for child in children:
            if not child in orig_children:
                changes.append(ChangeInfo(child, "created", "dir_collateral"))
    return changes
