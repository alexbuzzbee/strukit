"""
StruKit plain format module.
"""
from pathlib import Path
import re

KEY_REGEX = re.compile("^(.+): (.+)$")

debug = False

class PlainSyntaxError(Exception):
    """
    Invalid syntax was encountered while parsing the plain file.
    """
    pass

class Element:
    """
    Represents any element of data in a plain file. Abstract. Subclasses must implement __str__.
    """
    pass

class Key(Element):
    """
    Represents a key line.
    """
    key = ""
    value = ""

    def __str__(self):
        return self.key + ": " + self.value + "\n"
    
    def __init__(self, key, value):
        self.key = key
        self.value = value
    
class Body(Element):
    """
    Represents the body and separators.
    """
    text = ""

    def __str__(self):
        return "---\n" + self.text + "---\n"
    
    def __init__(self, text):
        self.text = text
    
class File:
    """
    Represents a plain file.
    """
    path: Path
    data = []
    keys = {}
    body: Body
    text = ""

    def add_key(self, key):
        """
        Add the specified Key to the file.
        """
        self.data.append(key)
        self.keys[key.key] = key

    def add_body(self, body):
        """
        Add the specified Body to the file. Only allowed once. Subsequent attempts will instead cause an exception to be raised.
        """
        if (not hasattr(self, "body")) or self.body == None:
            self.body = body
            self.data.append(body)
            if debug: print("DEBUG: body added")
        else:
            if debug: print("DEBUG: excess body")
            raise PlainSyntaxError("Multiple bodies")

    def remove_key(self, name):
        """
        Remove the key with the specified name from the file.
        """
        key = self.keys[name]
        del self.keys[name]
        self.data.remove(key)
    
    def parse(self):
        """
        Parse the file's text and generate appropriate data structures.
        """
        lines = self.text.split("\n")
        body_text = ""
        in_body = False
        for line in lines:
            if debug: print("DEBUG: parse '" + line + "'")
            
            # Separator.
            if line == "---":
                if debug: print("DEBUG: separator")
                in_body = not in_body
                if not in_body: # Add body at second separator.
                    body = Body(body_text)
                    self.add_body(body)
                continue

            # Body.
            if in_body:
                if debug: print("DEBUG: body")
                body_text = body_text + line + "\n"
                continue

            # Key.
            match = re.search(KEY_REGEX, line)
            if match:
                key = Key(match.group(1), match.group(2))
                if debug: print("DEBUG: key " + str(key))
                self.add_key(key)
                continue

            # Blank.
            if line == "":
                if debug: print("DEBUG: blank")
                continue

            if debug: print("DEBUG: bad syntax")
            raise PlainSyntaxError("General malformation")

        if debug: print("DEBUG: end of file")
        
        if in_body: # Add body at end of file.
            if debug: print("DEBUG: still in body")
            body = Body(body_text)
            self.add_body(body)

    def __str__(self):
        val = ""
        for element in self.data:
            val = val + str(element)
        return val

    def __getitem__(self, key):
        return self.keys[key].value
    
    def writeout(self):
        """
        Generate the file's text and write it out.
        """
        self.text = str(self)
        with self.path.open("w") as f:
            f.write(self.text)
        
    def __init__(self, path: Path, text=None):
        self.path = path
        self.data = []
        self.keys = {}

        if text: # If we've been given the text, we don't care about the existing file, if any.
            self.text = text
            self.parse()
        else:
            try:
                with self.path.open("r") as f:
                    self.text = f.read()
                    self.parse()
            except FileNotFoundError: # If the file is not found, we're creating a new one.
                pass
