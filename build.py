#!/usr/bin/env python3
"""
Builds StruKit zipapps from src/bin, including modules from src/lib when needed, and puts the results in build/bin.
"""
import tempfile
import shutil
import zipapp
from pathlib import Path

def tool(name, modules):
    """
    Build a zipapp for a StruKit tool.
    """
    with tempfile.TemporaryDirectory() as td:
        # Add main script to archive directory.
        shutil.copy2(Path("./src/bin") / (name + ".py"), Path(td) / "__main__.py")
        # Add all modules to archive directory.
        for mod in modules:
            shutil.copy2(Path("./src/lib") / (mod + ".py"), Path(td) / (mod + ".py"))
        # Build archive.
        zipapp.create_archive(td, Path("build/bin") / name, "/usr/bin/env python3")

bin = Path("build") / "bin"
for file in bin.iterdir():
    file.unlink()
bin.rmdir()
bin.mkdir(parents=True)

tool("plain2db", ["plain", "sqlnames"])
tool("db2plain", ["plain", "sqlnames"])
tool("plain2mail", ["plain"])
tool("plainrun", ["plain", "fwatch"])
tool("mail2plain", ["plain"])
tool("dbed", ["sqlnames"])
