# StruKit

**This project is not stable and should not be used in or near a production environment.**

StruKit is a "toolkit" of related, but not interdependent, tools written in Python for working with structured data of various types.

Most StruKit tools work with SQLite 3 databases (`db`) or with directories of semistructured text files (`plain`).

`plain` files consist of `key_name: value` pairs with an optional body wrapped in `---` separators, and are stored in directories under names corresponding to their first key's value.

An example of `plain`:

```
name: Example
type: Sample

---
This is example text in a StruKit text file.
---

original_date: 2020-02-03
```

## Tools

The following are tools included or planned to be included in StruKit:

### `libplain`

This tool is a small Python module for handling `plain` data. It is included as part of the other tools and does not need to be installed for them to work.

### `plain2db` and `db2plain`

These tools convert data between `plain` and `db` formats. A field called `Key-Name` in `plain` is called `key_name` in `db` and vice versa. The body is treated as a text field called `body`. The `db` tables must already exist and have compatible data types.

### `dbed`

This tool shows an interactive GUI form for editing `db` data. The form can be auto-generated or specified by a limited form language that specifies field labels and grid positions.

### `plain2mail` and `mail2plain`

These tools convert between `plain` format data and Unix mail. Fields named `To` and `Cc` will be used as the recipients, and the first field found of the following list will be used as the subject: `Subject`, `Title`, `Name`, `Label`. Fields so used will be deleted, but the rest will be included verbatim as the message body.

### `plainrun` and `dbrun`

These tools wait for new `plain` or `db` format data to appear, then run a command. The command can include the name or ID of the new data.

### `csv2plain` and `plain2csv`

These tools convert between `plain` format data and Comma-Separated Values and related formats.
